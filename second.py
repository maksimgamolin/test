from flask import Flask, abort, request, jsonify
import elasticsearch as ES

from validate import validate_args

app = Flask(__name__)


@app.route('/')
def index():
    return 'worked'

@app.route('/api/movies/')
def movie_list():
    # !! в зависимости от поддерживаемости проекта (сколько человек его будут поддерживать и будет ли он дорабатываться)
    # !!стоит либо использовать CBV и
    # !! соблюдая solid разрезать эту функцию на несколько https://habr.com/ru/company/mailru/blog/412699/
    validate = validate_args(request.args)

    if not validate['success']:
        return abort(422)

    defaults = {
        'limit': 50,
        'page': 1,
        'sort': 'id',
        'sort_order': 'asc'
    }

    # Тут уже валидно все
    # !! Можно заменить на defaults.update(request.args) https://docs-python.ru/tutorial/operatsii-slovarjami-dict-python/metod-dict-update/
    for param in request.args.keys():
        defaults[param] = request.args.get(param)

    # Уходит в тело запроса. Если запрос не пустой - мультисерч, если пустой - выдает все фильмы
    # !! Тернарный оператор так лучше не использовать, получается конструкция которую сложно читать
    body = {
        "query": {
            "multi_match": {
                "query": defaults['search'],
                "fields": ["title"]
            }
        }
    } if defaults.get('search', False) else {}

    # !! dict() лишняя операция луче сразу написать body['_source'] = {...}
    body['_source'] = dict()
    body['_source']['include'] = ['id', 'title', 'imdb_rating']

    params = {
        # !! ненужно оставлять закомментированный код
        # '_source': ['id', 'title', 'imdb_rating'],
        'from': int(defaults['limit']) * (int(defaults['page']) - 1),  # !! Возможно в validate сразу стоило привести к нужному типу
        'size': defaults['limit'],
        'sort': [
            {
                defaults["sort"]: defaults["sort_order"]
            }
        ]
    }
    # !! Соединение лучше открыть один раз, а не открывать каждый раз при запросе
    es_client = ES.Elasticsearch([{'host': '192.168.11.128', 'port': 9200}], )
    search_res = es_client.search(
        body=body,
        index='movies',
        params=params,
        filter_path=['hits.hits._source']
    )
    es_client.close()

    return jsonify([doc['_source'] for doc in search_res['hits']['hits']])


@app.route('/api/movies/<string:movie_id>')
def get_movie(movie_id):
    # !! Про соединение было выше
    es_client = ES.Elasticsearch([{'host': '192.168.11.128', 'port': 9200}], )

    if not es_client.ping():
        # !! в реальных приложениях стоит использовать библиотек logging https://habr.com/ru/post/144566/
        print('oh(')

    search_result = es_client.get(index='movies', id=movie_id, ignore=404)

    es_client.close()

    if search_result['found']:
        return jsonify(search_result['_source'])

    return abort(404)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
