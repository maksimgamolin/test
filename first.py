# !! мои коментарии начинаются вот так

import sqlite3
import json

from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
# !! форматирование импортов PEP https://www.python.org/dev/peps/pep-0008/#imports

def extract():
    # !!Мне кажется функцию стоило сделать генератором, и отдавать либо по одному значению, либо по чанкам
    # !!(список с несколькими знаениями), в противном случае можно очень много занять памяти без особой необходимости
    """
    extract data from sql-db
    :return:
    """
    # !! комментарий который ничего не комментирует, стоит избегать таких, либо описывать конкретнее\
    # что на вход функции приходит и что происходит.
    connection = sqlite3.connect("db.sqlite")
    cursor = connection.cursor()

    # Наверняка это пилится в один sql - запрос, но мне как-то лениво)
    # !! оператор joinпоможет все это вынестив один запрос
    # select
    #   m.id,
    #   m.imdb_rating,
    #   m.genre,
    #   m.title,
    #   m.plot,
    #   m.director,
    #   -- Через join присоединяем актеров и извлекаем имя
    #   (
    #     select
    #       GROUP_CONCAT(name)
    #     from
    #       (
    #         select
    #           a.name
    #         from
    #           movie_actors ma
    #           JOIN actors a on ma.actor_id = a.id
    #         where
    #           movie_id = m.id
    #       )
    #   ) actors,
    #   -- Тут посложнее, нужно массиву json объектов пройтись как по таблице json_each(m.writers, '$') и извлечь id json_extract(value, '$.id'), ну а дальше нехитрый join
    #   (
    #     case when m.writer <> '' then w.name else (
    #       select
    #         GROUP_CONCAT(w2.name)
    #       from
    #         writers w2
    #         join (
    #           select
    #             json_extract(value, '$.id') as writer_id
    #           from
    #             json_each(m.writers, '$')
    #         ) on w2.id = writer_id
    #     ) end
    #   ) writers
    # from
    #   movies m
    #   left join writers w on m.writer = w.id
    #!! Крайне рекоммендую использовать форматирование для sql кода, например https://codebeautify.org/sqlformatter

    # Получаем все поля для индекса, кроме списка актеров и сценаристов, для них только id
    #
    cursor.execute("""
        select id, imdb_rating, genre, title, plot, director,
        -- comma-separated actor_id's
        (
            select GROUP_CONCAT(actor_id) from
            (
                select actor_id
                from movie_actors
                where movie_id = movies.id
            )
        ),

        max(writer, writers)
        from movies
    """)

    # !! Вот в этом месте мы можем много потратить памяти, и именно тут стоило делать генератор,
    # !! брать понемногу и через yield возвращать
    raw_data = cursor.fetchall()

    # cursor.execute('pragma table_info(movies)')
    # pprint(cursor.fetchall())
    # !! Не стоит хранить закомментированный код, он либо есть в истории git либо никто уже не помнит зачем он нужен
    # !! подробней об этом можно прочесть в книге "Чистый код"

    # Нужны для соответсвия идентификатора и человекочитаемого названия
    # !! Нужно быть осторожнее с возвращением значений через звездочку, помимо того что мы можем гонять лишние значения,
    # !! что будет плохо сказываться на производительности и количестве требуемых ресурсов, еще схема данных может измениться,
    # !! тогда мы получим ошибку, но где-то ниже и будем тратить время на ее исправление
    actors = {row[0]: row[1] for row in cursor.execute('select * from actors where name != "N/A"')}
    writers = {row[0]: row[1] for row in cursor.execute('select * from writers where name != "N/A"')}
    # !!Если бы уложились в один запрос, то не потребовалось бы отдавать несколько значений, но если ситуция того требует
    # !! стоит посмотреть в сторону модуля namedtuple или создать dataclass, до кучи прописать тайпхинты к функции

    # !! Не вижу чтобы соединение было закрыто connection.close()
    return actors, writers, raw_data


def transform(__actors, __writers, __raw_data):
    # !! https://www.python.org/dev/peps/pep-0008/#function-and-method-arguments
    # !! Не думаю что 2 нижних подчеркивания перед названием аргумента необходимо,
    # !! 2 нижних подчеркивания перед названием используется для обозночения приватного поля
    """

    :param __actors:
    :param __writers:
    :param __raw_data:
    :return:
    """
    # !! Бесмысленный комментарий
    documents_list = []
    for movie_info in __raw_data:
        # Разыменование списка
        movie_id, imdb_rating, genre, title, description, director, raw_actors, raw_writers = movie_info

        if raw_writers[0] == '[':
            parsed = json.loads(raw_writers)
            # !! Мы делаем много лишних циклов по большим спискам, если нет возможности вытащить данные через один
            # !! запрос то мерж списков лучше проводить с использованием как моджно меньшего числа итераций
            # !! и сразу заполнить writers_list
            # !! кстати как вариант привести __writers и __actors к словарям и из них дергать значения,
            # !! скорость поиска в хеш таблицах (словарях) значительно быстрее

            new_writers = ','.join([writer_row['id'] for writer_row in parsed])
        else:
            new_writers = raw_writers

        writers_list = [(writer_id, __writers.get(writer_id)) for writer_id in new_writers.split(',')]
        actors_list = [(actor_id, __actors.get(int(actor_id))) for actor_id in raw_actors.split(',')]

        document = {
            "_index": "movies",
            "_id": movie_id,
            "id": movie_id,
            "imdb_rating": imdb_rating,
            "genre": genre.split(', '),
            "title": title,
            "description": description,
            "director": director,
            "actors": [
                {
                    "id": actor[0],
                    "name": actor[1]
                }
                for actor in set(actors_list) if actor[1]  # !! Это сразу можно было сформировать при запросе из базы
            ],
            "writers": [
                {
                    "id": writer[0],
                    "name": writer[1]
                }
                for writer in set(writers_list) if writer[1]  # !! Это сразу можно было сформировать при запросе из базы
            ]
        }
        # !! РЕкоммендую прочитать про http://old.code.mu/sql/distinct.html и про https://www.sqlite.org/json1.html#jmini

        # !! Я не видел где может быть 'N/A' но рекоммендую это обрабатывать еще на стадии формирования document
        for key in document.keys():
            if document[key] == 'N/A':
                # print('hehe')
                # !! Стоит удалить
                document[key] = None

        # !! опять же, цикл в цикле, это можно было обработать сразу при формировании document

        document['actors_names'] = ", ".join([actor["name"] for actor in document['actors'] if actor]) or None
        document['writers_names'] = ", ".join([writer["name"] for writer in document['writers'] if writer]) or None

        # !! import стоит писать вверху документа, на крайний случай если это точка останова дебагера
        # !! или pprint для отладки можно воспользоваться конструкцией import pprint; pprint.pprint(document)
        import pprint
        pprint.pprint(document)

        documents_list.append(document)

    return documents_list


def load(acts):
    """

    :param acts:
    :return:
    """
    # !! А что произойдет если при работе скрипт упадет? Например разорвется соединение,
    # !! такие  кейсы тоже стоит обрабатывать
    es = Elasticsearch([{'host': '192.168.1.252', 'port': 9200}])
    bulk(es, acts)

    return True


if __name__ == '__main__':
    load(transform(*extract()))
